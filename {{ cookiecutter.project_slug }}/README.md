# {{ cookiecutter.project_name }}

[![pipeline status](https://{{ cookiecutter.git_url }}/{{ cookiecutter.git_group }}/{{ cookiecutter.project_slug }}/badges/main/pipeline.svg)](https://{{ cookiecutter.git_url }}/{{ cookiecutter.git_group }}/{{ cookiecutter.project_slug }}/-/commits/main)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)


## Paper download

[![download pdf](https://img.shields.io/badge/Download-PDF-green)](https://{{ cookiecutter.git_url }}/{{ cookiecutter.git_group }}/{{ cookiecutter.project_slug }}/-/jobs/artifacts/main/file/tex/main.pdf?job=build_paper)


```bash
pip install pre-commit
pre-commit install

cd code

# Install pixi
curl -fsSL https://pixi.sh/install.sh | bash

# Install all dependencies
pixi install

# Activate a shell in the environment
pixi shell
# or source activate .pixi/envs/default

# Activate pre-commit
pre-commit install

# (Optional): launch jupyter notebook if not using vscode
jupyter notebook
```
