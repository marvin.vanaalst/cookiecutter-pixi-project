#!/bin/bash
# expects input like "main.tex"

NAME=$(echo $1$ | (cut -d "." -f 1))

pdflatex -interaction=nonstopmode --shell-escape "$1" || echo "failed, but continue running"
xdg-open "$NAME.pdf"
